#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

. /etc/os-release
case $ID in
debian|ubuntu)
    case $VERSION_CODENAME in
    bullseye|bookworm|focal|jammy|noble)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac


################
##            ##
##  DO TESTS  ##
##            ##
################

# TEST: dynamic linked binary
case $CODENAME in
bullseye|focal)
    ./dist/bullseye/amd64/nfpm --version | grep "GitVersion:    $PKG_VERSION"
    ;;
bookworm|jammy|noble)
    ./dist/bookworm/amd64/nfpm --version | grep "GitVersion:    $PKG_VERSION"
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac
./dist/bullseye/386/nfpm --version | grep "GitVersion:    $PKG_VERSION"
./dist/bookworm/386/nfpm --version | grep "GitVersion:    $PKG_VERSION"

# TEST: static linked binary
./dist/static/amd64/nfpm --version | grep "GitVersion:    $PKG_VERSION"
./dist/static/386/nfpm --version | grep "GitVersion:    $PKG_VERSION"

# TEST: install .deb package (dynamic)
apt-get install -y ./dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/nfpm --version | grep "GitVersion:    $PKG_VERSION"

# TEST: uninstall .deb package (dynamic)
apt-get autoremove --purge -y nfpm
test ! -x /usr/bin/nfpm

# TEST: upgrade .deb package (dynamic)
apt-get install -y ./test_data/nfpm_2.31.0-1-xdeb~${CODENAME}_amd64.deb
/usr/bin/nfpm --version | head -n1 | grep 'nfpm version nfpm version 2.31.0'
apt-get install -y ./dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/nfpm --version | grep "GitVersion:    $PKG_VERSION"

# TEST: install .deb package (static)
apt-get install -y ./dist/nfpm-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/nfpm --version | grep "GitVersion:    $PKG_VERSION"

# TEST: uninstall .deb package (static)
apt-get autoremove --purge -y nfpm-static
test ! -x /usr/bin/nfpm

# TEST: upgrade .deb package (static)
apt-get install -y ./test_data/nfpm-static_2.31.0-1-xdeb_amd64.deb
/usr/bin/nfpm --version | head -n1 | grep 'nfpm version nfpm version 2.31.0'
apt-get install -y ./dist/nfpm-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/nfpm --version | grep "GitVersion:    $PKG_VERSION"
