variables:
  PKG_NAME: nfpm
  PKG_VERSION: 2.41.3
  PKG_RELEASE: "1"
  TARGET_GOLANG_VERSION: 1.24.0

stages:
  - before build
  - build
  - package
  - test
  - before deploy
  - deploy
  - after deploy

cache-source-code:
  stage: before build
  image: alpine:latest
  before_script:
    - cat /etc/os-release
  script: ./ci-prepare-source-code.sh
  artifacts:
    expire_in: 1 week
    paths:
      - $CI_PROJECT_DIR/${PKG_NAME}-${PKG_VERSION}/
  tags: [ download ]

build-bookworm-based-dynamic:
  stage: build
  image: golang:${TARGET_GOLANG_VERSION}-bookworm
  variables:
    GOCACHE: ${CI_PROJECT_DIR}/.cache/go-build
    GOMODCACHE: ${CI_PROJECT_DIR}/.cache/go-mod
    DEBIAN_FRONTEND: noninteractive
  before_script: &setup-building-env
    - cat /etc/os-release
    - go version
  script: &building-script
    - ./ci-build.sh
  cache: &building-cache
    key: ${PKG_NAME}-${PKG_VERSION}-bookworm
    paths:
      - .cache/go-build
      - .cache/go-mod
  dependencies: &building-dependencies
    - cache-source-code
  artifacts: &artifacts
    expire_in: 1 week
    paths:
      - dist/*.apk
      - dist/*.deb
      - dist/*/386/nfpm
      - dist/*/amd64/nfpm

build-bullseye-based-dynamic:
  stage: build
  image: golang:${TARGET_GOLANG_VERSION}-bullseye
  variables:
    GOCACHE: ${CI_PROJECT_DIR}/.cache/go-build
    GOMODCACHE: ${CI_PROJECT_DIR}/.cache/go-mod
    DEBIAN_FRONTEND: noninteractive
  before_script: *setup-building-env
  script: *building-script
  cache:
    <<: *building-cache
    key: ${PKG_NAME}-${PKG_VERSION}-bullseye
  dependencies: *building-dependencies
  artifacts: *artifacts

build-static:
  stage: build
  image: golang:${TARGET_GOLANG_VERSION}-alpine
  variables:
    GOCACHE: ${CI_PROJECT_DIR}/.cache/go-build
    GOMODCACHE: ${CI_PROJECT_DIR}/.cache/go-mod
  before_script: *setup-building-env
  script:
    - apk add bash
    - ./ci-build.sh
  cache:
    <<: *building-cache
    key: ${PKG_NAME}-${PKG_VERSION}-alpine
  dependencies: *building-dependencies
  artifacts: *artifacts

package:
  stage: package
  image: alpine:latest
  script:
    # Do nothing, but sibling project need this job to download .deb files
    # instead of download from multi jobs.
    - cat /etc/os-release
    - apk add tree
    - tree . # print file tree for debugging
  dependencies:
    - build-bullseye-based-dynamic
    - build-bookworm-based-dynamic
    - build-static
  artifacts: *artifacts
  tags: [ package ]

test-bookworm:
  stage: test
  image: debian:bookworm
  script: &testing-script
    - apt-get update
    - apt-get install -y tree
    - tree .
    - ./ci-test-dynamic.sh
  dependencies: [ package ]
  tags: [ download ]

test-bullseye:
  stage: test
  image: debian:bullseye
  script: *testing-script
  dependencies: [ package ]
  tags: [ download ]

test-focal:
  stage: test
  image: ubuntu:focal
  script: *testing-script
  dependencies: [ package ]
  tags: [ download ]

test-jammy:
  stage: test
  image: ubuntu:jammy
  script: *testing-script
  dependencies: [ package ]
  tags: [ download ]

test-noble:
  stage: test
  image: ubuntu:noble
  script: *testing-script
  dependencies: [ package ]
  tags: [ download ]

test-alpine:
  stage: test
  image: alpine:latest
  script:
    - apk add bash tree
    - tree .
    - ./ci-test-static.sh
  dependencies: [ package ]
  tags: [ download ]

pages-content:
  stage: before deploy
  image: debian:latest
  variables:
    DEBIAN_FRONTEND: noninteractive
  before_script:
    - apt-get update
    - apt-get install --yes gpg reprepro tree
    - tree . # print file tree for debugging
  script:
    - set -eu

    # import the sign key
    - test -n "$PRIVATE_SIGKEY"  # ensure $PRIVATE_SIGKEY is not empty
    - echo "$PRIVATE_SIGKEY" | base64 -d | gpg --import
    - gpg -K
    # add .deb files into Debian repository
    - |-
      for codename in bullseye bookworm; do
        reprepro -b debian -C contrib includedeb $codename dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${codename}_*.deb
        reprepro -b debian -C contrib includedeb $codename dist/${PKG_NAME}-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_*.deb
      done
    # add .deb files into Ubuntu repository
    - |-
      for codename in focal jammy noble; do
        reprepro -b ubuntu -C universe includedeb $codename dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${codename}_*.deb
        reprepro -b ubuntu -C universe includedeb $codename dist/${PKG_NAME}-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_*.deb
      done
    # publish
    - mkdir -p public
    - cp -v pubkey.asc pubkey.gpg public/
    - cp -v -r debian public/
    - cp -v -r ubuntu public/
    - cp -v dist/*.apk public/
    - tar -cC dist/bullseye/amd64 ${PKG_NAME} | gzip -9 - > public/${PKG_NAME}_amd64.tar.gz
    - tar -cC dist/bullseye/386   ${PKG_NAME} | gzip -9 - > public/${PKG_NAME}_i386.tar.gz
    - tar -cC dist/static/amd64   ${PKG_NAME} | gzip -9 - > public/${PKG_NAME}-static_amd64.tar.gz
    - tar -cC dist/static/386     ${PKG_NAME} | gzip -9 - > public/${PKG_NAME}-static_i386.tar.gz
    # make `index.html`
    - tree public -H '.' -T "${title:-$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME}" -I "reveal*|img*|style*|index*|content*|data" --noreport --charset utf-8 > public/index.html
  dependencies: [ package ]
  artifacts: &pages-artifacts
    expire_in: 1 week
    paths: [ public ]
  tags: [ pages ]

pages:
  stage: deploy
  image: alpine:latest
  script:
    # do nothing, but copy pages-content artifacts here to make deploy
    - cat /etc/os-release
    - apk add tree
    - tree .
  dependencies: [ pages-content ]
  artifacts: *pages-artifacts
  only: [ main ]
  tags: [ pages ]

container-registry:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:${PKG_VERSION}" .
    - docker tag "$CI_REGISTRY_IMAGE:${PKG_VERSION}" "$CI_REGISTRY_IMAGE:latest"
    - docker push --all-tags "$CI_REGISTRY_IMAGE"
  tags: [ docker-registry ]

trigger-xdeb-debian:
  stage: after deploy
  trigger: xdeb/debian
  only: [ main ]

trigger-xdeb-ubuntu:
  stage: after deploy
  trigger: xdeb/ubuntu
  only: [ main ]

# vim: ts=2 sw=2 et
