#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\e[31m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\e[31m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


#########################
#  Set up building env  #
#########################

. /etc/os-release
case $ID in
debian)
    case $VERSION_CODENAME in
    bullseye|bookworm)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
alpine)
    CODENAME=static
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac
export CODENAME

src_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION
dist_dir=$CI_PROJECT_DIR/dist/$CODENAME

# Prepare `envsubst` command
case $CODENAME in
bullseye|bookworm)
    apt-get update
    apt-get install --no-install-recommends --yes gettext
    ;;
static)
    apk add gettext
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

# Prepare `file` command
case $CODENAME in
bullseye|bookworm)
    apt-get update
    apt-get install -y file
    ;;
static)
    apk add file
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

cd $src_dir


# #############
# #  DO TEST  #
# #############
# # FIXME: do a real TEST.
# go fmt $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go vet $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go test -race $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"


##############
#  DO BUILD  #
##############

# Initial var `ldflags`
case $CODENAME in
bullseye|bookworm)
    test 1 -eq $(go env CGO_ENABLED)  # ensure CGO_ENABLED=1
    ldflags='-s -w'
    ;;
static)
    test 0 -eq $(go env CGO_ENABLED)  # ensure CGO_ENABLED=0
    ldflags='-s -w -extldflags="-static"'
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

# Build binary output
ldflags="$ldflags
    -X 'main.version=$PKG_VERSION'
    -X 'main.treeState=clean'
    -X 'main.commit=n/a'
    -X 'main.date=$(date -u +%F\ %T\ %Z)'
    -X 'main.builtBy=https://gitlab.com/xdeb/nfpm'"
GOARCH=amd64 go build -o $dist_dir/amd64/nfpm -ldflags="$ldflags" -trimpath ./cmd/nfpm
GOARCH=386 go build -o $dist_dir/386/nfpm -ldflags="$ldflags" -trimpath ./cmd/nfpm
case $CODENAME in
bullseye|bookworm)
    ldd $dist_dir/amd64/nfpm  # check if `$dist_dir/amd64/nfpm` is a dynamic linked binary
    ! ldd $dist_dir/386/nfpm  # check if `$dist_dir/386/nfpm` is not a dynamic linked binary, since it's cross built.
    ;;
static)
    ! ldd $dist_dir/amd64/nfpm  # check if `$dist_dir/amd64/nfpm` is not a dynamic linked binary
    ! ldd $dist_dir/386/nfpm    # check if `$dist_dir/386/nfpm` is not a dynamic linked binary
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac
file $dist_dir/amd64/nfpm $dist_dir/386/nfpm


################
#  DO PACKING  #
################

# Prepare files
cd "$CI_PROJECT_DIR"
mkdir -p dist/noarch
cp -v $src_dir/README.md  dist/noarch/README.md
cp -v $src_dir/LICENSE.md dist/noarch/LICENSE.md
$dist_dir/amd64/nfpm man > dist/noarch/nfpm.1 && gzip -9fv dist/noarch/nfpm.1
$dist_dir/amd64/nfpm completion bash > dist/noarch/nfpm.bash
$dist_dir/amd64/nfpm completion fish > dist/noarch/nfpm.fish
$dist_dir/amd64/nfpm completion zsh  > dist/noarch/nfpm.zsh

# build .deb package file
case $CODENAME in
bullseye)
    PKG_ARCH=amd64 TARGET_CODENAME=bullseye envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bullseye envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=amd64 TARGET_CODENAME=focal    envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=focal    envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    ;;
bookworm)
    PKG_ARCH=amd64 TARGET_CODENAME=bookworm envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bookworm envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=amd64 TARGET_CODENAME=jammy    envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=jammy    envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=amd64 TARGET_CODENAME=noble    envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=noble    envsubst < dynamic.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    ;;
static)
    PKG_NAME="$PKG_NAME-static" PKG_ARCH=amd64 envsubst < static.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_NAME="$PKG_NAME-static" PKG_ARCH=386   envsubst < static.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=amd64 envsubst < static.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p apk -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   envsubst < static.nfpm.yaml | $dist_dir/amd64/nfpm pkg -p apk -t $dist_dir/.. -f /dev/stdin
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

# Check outputs
ls -lAFR --color $CI_PROJECT_DIR/dist/ $CI_PROJECT_DIR/dist/*/
