#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\e[31m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\e[31m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


# TEST: run binary file (x86_64)
./dist/static/amd64/nfpm --version | head -n1 | grep "^nfpm version nfpm version $PKG_VERSION$"

# TEST: run binary file (x86)
./dist/static/386/nfpm --version | head -n1 | grep "^nfpm version nfpm version $PKG_VERSION$"

# TEST: install .apk package (x86_64)
apk add --allow-untrusted ./dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86_64.apk
/usr/bin/nfpm --version | head -n1 | grep "^nfpm version nfpm version $PKG_VERSION$"
apk del nfpm

# TEST: install .apk package (x86)
apk add --allow-untrusted ./dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86.apk
/usr/bin/nfpm --version | head -n1 | grep "^nfpm version nfpm version $PKG_VERSION$"
apk del nfpm
