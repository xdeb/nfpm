FROM alpine:latest
ADD ./dist/static/amd64/nfpm /usr/bin/nfpm
RUN set -eux ;\
    apk add --no-cache gettext ;\
    cat /etc/os-release ;\
    nfpm --version
