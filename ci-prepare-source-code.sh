#!/bin/sh

set -eux


tarball_url=https://github.com/goreleaser/nfpm/archive/refs/tags/v$PKG_VERSION.tar.gz
tarball_path=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION.tar.gz
src_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION


test ! -e "$tarball_path" && wget --no-verbose -O "$tarball_path" "$tarball_url"

tar -xf $tarball_path

# check if the tarball was extracted in `$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION` directory
test -d $src_dir

ls -lAF
