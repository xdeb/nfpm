#!/bin/bash

. ./make_test_data.env
PS4='\n\[\e[31m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'

set -eux

srcdir=$CI_PROJECT_DIR/nfpm-$PKG_VERSION
pwd=$(pwd)
volname_cache_gobuild=cache-$PKG_NAME-gobuild
volname_cache_pkgmod=cache-$PKG_NAME-pkgmod


# Prepare source code tarball file
test -e /tmp/nfpm-$PKG_VERSION.tar.gz || wget -O /tmp/nfpm-$PKG_VERSION.tar.gz https://github.com/goreleaser/nfpm/archive/refs/tags/v$PKG_VERSION.tar.gz


############################################
##                                        ##
##  CI JOB: build-bullseye-based-dynamic  ##
##                                        ##
############################################

ctname=nfpm-builder-bullseye

# Set up builder container
docker rm -f $ctname
docker run --name=$ctname -d -t \
    -v $volname_cache_gobuild:/root/.cache/go-build \
    -v $volname_cache_pkgmod:/go/pkg/mod \
    -v /tmp/nfpm-$PKG_VERSION.tar.gz:$CI_PROJECT_DIR/nfpm-$PKG_VERSION.tar.gz:ro \
    -w $CI_PROJECT_DIR \
    -h $ctname \
    --env-file make_test_data.env \
    -e DEBIAN_FRONTEND=noninteractive \
    golang:$GOVERSION-bullseye
docker exec -it $ctname tar -xf nfpm-$PKG_VERSION.tar.gz

# Use local comstomized apt.conf to speed up `apt-get install`, if posible
test -e /etc/apt/apt.conf && docker cp /etc/apt/apt.conf $ctname:/etc/apt/apt.conf

# Use local comstomized goproxy settings to speed up downloading, if posible
test -n "$(go env GOPROXY)" &&
    docker exec -it $ctname go env -w GOPROXY="$(go env GOPROXY)"
set +u
test -n "$GOPROXY" &&
    # $GOPROXY is higher than $(go env GOPROXY) in priority
    docker exec -it $ctname go env -w GOPROXY="$GOPROXY"
set -u

# Simulate `git clone`: Copy all source files from the repository into the container.
sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

# Patching nfpm.yaml and static-nfpm.yaml to avoid signature
docker exec -it $ctname sed -i '/^# import the sign key$/{N;N;d;}' $CI_PROJECT_DIR/ci-build.sh
docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm.yaml

# Do build
docker exec -it $ctname ./ci-build.sh

# Gain artifacts
docker exec -it $ctname bash -c 'tar -czf artifacts.tar.gz dist/*/{amd64,386}/nfpm dist/*.deb'
docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz


############################################
##                                        ##
##  CI JOB: build-bookworm-based-dynamic  ##
##                                        ##
############################################

ctname=nfpm-builder-bookworm

# Set up builder container
docker rm -f $ctname
docker run --name=$ctname -d -t \
    -v $volname_cache_gobuild:/root/.cache/go-build \
    -v $volname_cache_pkgmod:/go/pkg/mod \
    -v /tmp/nfpm-$PKG_VERSION.tar.gz:$CI_PROJECT_DIR/nfpm-$PKG_VERSION.tar.gz:ro \
    -w $CI_PROJECT_DIR \
    -h $ctname \
    --env-file make_test_data.env \
    -e DEBIAN_FRONTEND=noninteractive \
    golang:$GOVERSION-bookworm
docker exec -it $ctname tar -xf nfpm-$PKG_VERSION.tar.gz

# Use local comstomized apt.conf to speed up `apt-get install`, if posible
test -e /etc/apt/apt.conf && docker cp /etc/apt/apt.conf $ctname:/etc/apt/apt.conf

# Use local comstomized goproxy settings to speed up downloading, if posible
test -n "$(go env GOPROXY)" &&
    docker exec -it $ctname go env -w GOPROXY="$(go env GOPROXY)"
set +u
test -n "$GOPROXY" &&
    # $GOPROXY is higher than $(go env GOPROXY) in priority
    docker exec -it $ctname go env -w GOPROXY="$GOPROXY"
set -u

# Simulate `git clone`: Copy all source files from the repository into the container.
sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

# Patching nfpm.yaml and static-nfpm.yaml to avoid signature
docker exec -it $ctname sed -i '/^# import the sign key$/{N;N;d;}' $CI_PROJECT_DIR/ci-build.sh
docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm.yaml

# Do build
docker exec -it $ctname ./ci-build.sh

# Gain artifacts
docker exec -it $ctname bash -c 'tar -czf artifacts.tar.gz dist/*/{amd64,386}/nfpm dist/*.deb'
docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz


############################
##                        ##
##  CI JOB: build-static  ##
##                        ##
############################

ctname=nfpm-builder-static

# Set up builder container
docker rm -f $ctname
docker run --name=$ctname -d -t \
    -v $volname_cache_gobuild:/root/.cache/go-build \
    -v $volname_cache_pkgmod:/go/pkg/mod \
    -v /tmp/nfpm-$PKG_VERSION.tar.gz:$CI_PROJECT_DIR/nfpm-$PKG_VERSION.tar.gz:ro \
    -w $CI_PROJECT_DIR \
    -h $ctname \
    --env-file make_test_data.env \
    golang:$GOVERSION-alpine
docker exec -it $ctname sed -i s/https:/http:/ /etc/apk/repositories
docker exec -it $ctname apk add bash
docker exec -it $ctname tar -xf nfpm-$PKG_VERSION.tar.gz

# Use local comstomized goproxy settings to speed up downloading
test -n "$(go env GOPROXY)" &&
    docker exec -it $ctname go env -w GOPROXY="$(go env GOPROXY)"
set +u
test -n "$GOPROXY" &&
    # $GOPROXY is higher than $(go env GOPROXY) in priority
    docker exec -it $ctname go env -w GOPROXY="$GOPROXY"
set -u

# Simulate `git clone`: Copy all source files from the repository into the container.
sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

# Patching nfpm.yaml and static-nfpm.yaml to avoid signature
docker exec -it $ctname sed -i '/^# import the sign key$/{N;N;d;}' $CI_PROJECT_DIR/ci-build.sh
docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm-static.yaml

# Do build
docker exec -it $ctname ./ci-build.sh

# Gain artifacts
docker exec -it $ctname bash -c 'tar -czf artifacts.tar.gz dist/*/{amd64,386}/nfpm dist/*.{deb,apk}'
docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz


################
##            ##
##  DO TESTS  ##
##            ##
################

for codename in bullseye bookworm focal jammy noble
do
    case $codename in
        bullseye|bookworm) os=debian ;;
        focal|jammy|noble) os=ubuntu ;;
    esac

    set +x
    echo "*********************************************************************************"
    echo "**  CI JOB: test-$codename                                                     **"
    echo "*********************************************************************************"
    set -x
    docker run --rm -it \
        -v $pwd/test_data/ci-test-dynamic.sh:/bin/ci-test-dynamic.sh:ro \
        -v /tmp/nfpm-builder-bullseye_artifacts.tar.gz:/tmp/nfpm-builder-bullseye_artifacts.tar.gz:ro \
        -v /tmp/nfpm-builder-bookworm_artifacts.tar.gz:/tmp/nfpm-builder-bookworm_artifacts.tar.gz:ro \
        -v /tmp/nfpm-builder-static_artifacts.tar.gz:/tmp/nfpm-builder-static_artifacts.tar.gz:ro \
        -h test-$codename \
        --env-file ./make_test_data.env \
        -e DEBIAN_FRONTEND=noninteractive \
        $os:$codename sh -c "set -eux
        tar -xf /tmp/nfpm-builder-bullseye_artifacts.tar.gz
        tar -xf /tmp/nfpm-builder-bookworm_artifacts.tar.gz
        tar -xf /tmp/nfpm-builder-static_artifacts.tar.gz
        ci-test-dynamic.sh"
done

set +x
echo "*********************************************************************************"
echo "**  CI JOB: test-alpine                                                        **"
echo "*********************************************************************************"
set -x
docker run --rm -it \
    -v $pwd/test_data/ci-test-static.sh:/bin/ci-test-static.sh:ro \
    -v /tmp/nfpm-builder-static_artifacts.tar.gz:/tmp/nfpm-builder-static_artifacts.tar.gz:ro \
    -h test-alpine \
    --env-file ./make_test_data.env \
    alpine:latest sh -c "set -eux
    tar -xf /tmp/nfpm-builder-static_artifacts.tar.gz
    sed -i s/https:/http:/ /etc/apk/repositories
    apk add bash
    ci-test-static.sh"


################################
##                            ##
##  COPY OUT .DEB, .APK FILE  ##
##                            ##
################################

mkdir -p test_data
rm -fv test_data/*.deb test_data/*.apk

tmpdir=$(mktemp -d)
tar -C $tmpdir -xf /tmp/nfpm-builder-bullseye_artifacts.tar.gz
tar -C $tmpdir -xf /tmp/nfpm-builder-bookworm_artifacts.tar.gz
tar -C $tmpdir -xf /tmp/nfpm-builder-static_artifacts.tar.gz
cp -v $tmpdir/dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bullseye_amd64.deb \
    $tmpdir/dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb~focal_amd64.deb \
    $tmpdir/dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bookworm_amd64.deb \
    $tmpdir/dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb~jammy_amd64.deb \
    $tmpdir/dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb~noble_amd64.deb \
    $tmpdir/dist/nfpm-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb \
    $tmpdir/dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86_64.apk \
    $tmpdir/dist/nfpm_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86.apk \
    test_data/
rm -rf $tmpdir
